#!/bin/bash

DIR=$1 # working directory where the D00, D07 and D14 directory are located
day=$2 #pictures taken after how many days after herbicide application? 00, 07 or 14
EXTENSION_NAME=$3 #extension names of the images, e.g JPG, jpg or png
# SIXBYSEVEN=$3 #are the picture conforming to the 6x7 tray arrangement? 0 for no and 1 for yes
#NOTE!:
# Be sure to keep only the final set of plant photographs on the D00, D07 and D14 directories

#rename pictures (each picture contains at most one plant)
cd ${DIR}/D${day}/
mkdir INPUT/ #make the main INPUT directory: renamed according to cell ID
mkdir RAW_IMAGES/ #make the main RAW_IMAGES directory: original names

#################################
###							  ###
### 6 x 7 Picture Arrangement ###
###							  ###
#################################
#1: list all JPG files we will rename
find | grep .${EXTENSION_NAME} | sort > images.list

#2: remove marker pictures manually while checking for duplications or ommisions during photographing

#3: generate filenames list
nimages=$(wc -l images.list | cut -d' ' -f1)
ntrays=$(Rscript -e $(echo "ceiling($nimages/(4*42))") | cut -d' ' -f2) #count the number of 48-cell trays ceiling of n/48
touch filenames.list

for i in $(seq 1 $ntrays)
do
for j in $(seq 1 7)
do
for k in A B C D E F
do
for l in $(seq 1 4)
do
if [ $i -lt 10 ]
then
echo -e "DAY$day-TRAY0$i-$j$k-REP$l.jpg" >> filenames.list
else
echo -e "DAY$day-TRAY$i-$j$k-REP$l.jpg" >> filenames.list
fi
done
done
done
done

#4: check if we have the same number of expected pictures (filenames.list) and the available pictures (images.list)
wc -l filenames.list
wc -l images.list

#5: rename!
for image in $(seq 1 $(wc -l images.list | cut -d' ' -f1))
do
oldName=$(head -n$image images.list | tail -n1)
newName=$(head -n$image filenames.list | tail -n1)
cp $oldName INPUT/${newName}
mv $oldName RAW_IMAGES/
echo $image
done

rm filenames.list images.list

# #################################
# ###							  ###
# ###  Generic Renaming with 4  ###
# ###	     Technical Reps		  ###
# ###							  ###
# #################################
# #for images not conforming to the 6x7 tray arrangement
# find | grep .JPG | sort > images.list
# touch filenames.list
# for i in $(seq 1 $(echo $(wc -l images.list | cut -d$' ' -f1) / 4 | bc))
# do
# for j in $(seq 1 4)
# do
# if [ $i -lt 10 ]
# then
# echo -e "DAY${day}-PLANT0${i}-REP$j.jpg" >> filenames.list
# else
# echo -e "DAY${day}-PLANT${i}-REP$j.jpg" >> filenames.list
# fi
# done
# done

# #rename
# mkdir INPUT/ #make the main INPUT directory
# for image in $(seq 1 $(wc -l filenames.list | cut -d' ' -f1))
# do
# oldName=$(head -n$image images.list | tail -n1)
# newName=$(head -n$image filenames.list | tail -n1)
# cp $oldName INPUT/${newName}
# echo $image
# done

# mkdir RAW_IMAGES
# mv *.JPG RAW_IMAGES
# rm images.list filenames.list

# rm -R RAW_IMAGES

# mv INPUT/*.jpg .
# rm -R INPUT

#Example for loop across different populations
#for i in $(ls /mnt/PREDICTION_20180926_VIC_POP/); do ./rename_pictures.sh /mnt/PREDICTION_20180926_VIC_POP/${i} 00 jpg; done