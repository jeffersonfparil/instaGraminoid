i=11 #8, 11, 12, 13, 23
coordinates = contours[i] 
x,y,w,h = cv2.boundingRect(coordinates)
area = w*h
#if area >= LEAF_DETECTION_THRESHOLD:
leaf = imgExtract[y:y+h, x:x+w]
plt.imshow(cv2.cvtColor(leaf, cv2.COLOR_BGR2RGB)); plt.show()
#GREENESS METRICS: (1) AUC for 100-150 interval; (2) parameter estimation using mixed normal distribution; (3) ( G/R + (1-(B+G+R/2*255)) ) / 2 

#...............#
#...............#
#...............#
#...............#
#...............#
#...............#

######################
# convert green channel pixel values into density
green_px = [i for sub in leaf[:,:,1] for i in sub]
blue_px = [i for sub in leaf[:,:,0] for i in sub]
red_px = [i for sub in leaf[:,:,2] for i in sub]
filtered_green = filter(lambda a:a!=0, green_px)
filtered_red = filter(lambda a:a!=0, red_px)
filtered_blue = filter(lambda a:a!=0, blue_px)

# ###
# #testing manual density function
# counts = np.histogram(filtered, bins=255)
# density = counts[0].astype(float) / range(2,257)
# plt.plot(density); plt.show()
# ###
DATA_green = pd.DataFrame(filtered_green, columns=['data'])
density_green = stats.kde.gaussian_kde(DATA_green.values.ravel())
x = np.linspace(0, 255, 255)
density_green = density_green(x)

DATA_red = pd.DataFrame(filtered_red, columns=['data'])
density_red = stats.kde.gaussian_kde(DATA_red.values.ravel())
x = np.linspace(0, 255, 255)
density_red = density_red(x)

DATA_blue = pd.DataFrame(filtered_blue, columns=['data'])
density_blue = stats.kde.gaussian_kde(DATA_blue.values.ravel())
x = np.linspace(0, 255, 255)
density_blue = density_blue(x)

np.mean(DATA_green)
np.mean(DATA_red)
np.mean(DATA_blue)

plt.plot(density_green); plt.show()
plt.plot(density_red); plt.show()
plt.plot(density_blue); plt.show()

DATA = density

###########################

plt.imshow(cv2.cvtColor(leaf, cv2.COLOR_BGR2RGB)); plt.show(); plt.plot(density); plt.show()

################## LEAST SQUARES METHOD:
#for testing:(density model ~ mixed 2 normal distributions)
p=0.8
u1, u2 = 120, 160
s1, s2 = 5, 10
size=100
DATA = (p*norm.pdf(range(1,256), loc=u1, scale=s1)) + ((1-p)*norm.pdf(range(1,256), loc=u2, scale=s2))
plt.plot(DATA); plt.show()
#################
#defining the loss function:
def mixed_distn(param):
	p, s1, s2 = param
	u1, u2 = 120, 160
	ESTIMATE = (p*norm.pdf(range(1,256), loc=u1, scale=s1)) + ((1-p)*norm.pdf(range(1,256), loc=u2, scale=s2))
	return sum((DATA-ESTIMATE)**2)
	#return 1-(np.corrcoef(DATA, ESTIMATE)[0,1])

###################
#param_estimation! #### I may be missing something here with the minimize function..... --> FOUND IT! SET FIXED MEANS! Since: ab = ba, duh hahaha! 20171026
initial_param = [0.05,0.01,0.01]
param_bounds = ((0,1), (1,1000), (1,1000))
out = minimize(mixed_distn, x0=initial_param, bounds=param_bounds)

###################
#checking
print(out.x)
p_est, s1_est, s2_est = out.x
u1, u2 = 120, 160
estimated_DATA = (p_est*norm.pdf(range(1,256), loc=u1, scale=s1_est)) + ((1-p_est)*norm.pdf(range(1,256), loc=u2, scale=s2_est))
plt.plot(DATA); plt.show()
plt.plot(estimated_DATA); plt.show()
np.corrcoef(DATA, estimated_DATA)


################## MAXIMUM LIEKLIHOOD METHOD:
#for testing:(density model ~ mixed 2 normal distributions)
p=0.8
u1, u2 = 120, 160
s1, s2 = 5, 10
size=500
probDATA = (p*norm.pdf(range(1,256), loc=u1, scale=s1)) + ((1-p)*norm.pdf(range(1,256), loc=u2, scale=s2))
DATA = np.random.choice(range(1,256), size=size, replace=True, p=probDATA)
plt.plot(DATA); plt.show()

forHIST = pd.DataFrame(DATA, columns=['data'])
density = stats.kde.gaussian_kde(forHIST.values.ravel())
domain = np.linspace(0, 255, 255)
density = density(domain)
plt.plot(density); plt.show()

#################
#defining the -loglikelihood function function:
def mixture_distn(param):
	likelihood = -np.log10( (param[0]*norm.pdf(DATA, loc=120, scale=param[1])) + ((1-param[0])*norm.pdf(DATA, loc=160, scale=param[2])) )
	if np.sum(likelihood) == float('Inf'):
		return 1e06
	else:
		return np.sum(likelihood)


###################
#maximum likelihood parameter estimation
initial_param = [0.0001,1,1]
param_bounds = ((0.0001,0.9999), (1,1000), (1,1000))
#out = minimize(mixture_distn, x0=initial_param, bounds=param_bounds, method='L-BFGS-B')
#DATA = filtered
minimize(mixture_distn, x0=initial_param, bounds=param_bounds, method='L-BFGS-B', options={'disp':True})

###################
#checking
print(out.x)
p_est, s1_est, s2_est = out.x
u1, u2 = 120, 160
estimated_DATA = (p_est*norm.pdf(range(1,256), loc=u1, scale=s1_est)) + ((1-p_est)*norm.pdf(range(1,256), loc=u2, scale=s2_est))
plt.plot(DATA); plt.show()
plt.plot(estimated_DATA); plt.show()
np.corrcoef(DATA, estimated_DATA)


################### 
##
## USING HSV colorspace instead of BGR
##
##################

hsv = cv2.cvtColor(leaf, cv2.COLOR_BGR2HSV)
hist = cv2.calcHist([hsv],[0],None,[359],[1,360])
plt.plot(hist); plt.show()

###########################
#Rscript
mixture <-function(data, param){
	logLik = -log( (param[1]*dnorm(data, mean=120, sd=param[2])) + ((1-param[1])*dnorm(data, mean=160, sd=param[3])) )
	if(is.infinite(sum(logLik))) {
		return(1000000000)
	} else {
		return(sum(logLik))
	}
}
x=optim(par=c(0.0001, 1, 1), mixture, data=as.numeric(DAT), lower=c(0,1,1), upper=c(1, 1000,1000), method='L-BFGS-B')
