###########################
### Building Resistance ###
###     Landscapes      ###
###########################

### load libraries
library(akima)
library(mgcv)
library(maps)

### load Lolium collection passport data
passport = read.table("Lolium_collection_passport_data.txt", sep="\t", header=TRUE)

### herbicide names: empirical and predicted
HERBI_LIST_PLUS_PREDCITED = colnames(passport)[9:ncol(passport)]
HERBI_LIST = HERBI_LIST_PLUS_PREDCITED[!grepl("PREDICTED", HERBI_LIST_PLUS_PREDCITED)]
PREDICTED_LIST = HERBI_LIST_PLUS_PREDCITED[grepl("PREDICTED", HERBI_LIST_PLUS_PREDCITED)]
PREDICTED_LIST_CORRESPONDING_HERBI = matrix(unlist(strsplit(PREDICTED_LIST, "PREDICTED_")), nrow=length(PREDICTED_LIST), byrow=TRUE)[,2]

### iterate across herbicides
orig_par = par(no.readonly=TRUE)
for (herbi in HERBI_LIST) {
  print(herbi)
  # herbi = HERBI_LIST[2]
  ### merge empirical and predicted resistances
  idx = !is.na(eval(parse(text=paste0("passport$PREDICTED_", herbi))))
  eval(parse(text=paste0("passport$", herbi, "[idx] = passport$PREDICTED_", herbi, "[idx]")))
  passport$pch = rep(21, times=nrow(passport)) ### circular points for empirical data
  passport$pch[idx] = 24 ### triangular points for predicted data
  ### filter
  filtered = passport[complete.cases(eval(parse(text=paste0("passport$", herbi)))),]
  filtered = eval(parse(text=paste0("filtered[order(filtered$", herbi,"), ]")))
  x=filtered$Coordinates_E
  y=filtered$Coordinates_N
  z = eval(parse(text=paste0("filtered$", herbi)))/100
  # standardize z
  muz=mean(z, na.rm=TRUE); sdz=sd(z, na.rm=TRUE)
  z = (z-muz)/sdz
  ### interpolate given empirical data
  nextrap = 100
  interpolated = akima::interp(x=x, y=y, z=z, duplicate="mean", nx=nextrap, ny=nextrap, linear=TRUE)
  newx = rep(interpolated$x, each=length(interpolated$y))
  newy = rep(interpolated$y, times=length(interpolated$x))
  newz = as.vector(t(interpolated$z))
  ### perform gam fittings
  mod_SPLINE = mgcv::gam(newz ~ newx + newy + s(newx,newy), method="REML")
  mod_TENSOR = mgcv::gam(newz ~ newx + newy + te(newx,newy), method="REML")
  ### gams on the original (unimputed/uninterpolated) data fro R2 reporting
  tryCatch({
    mod_SPLINE_orig = mgcv::gam(z ~ x + y + s(x,y), method="REML")
    mod_TENSOR_orig = mgcv::gam(z ~ x + y + te(x,y), method="REML")
  }, error=function(e) {
    tryCatch({
      x_ad = c(x, newx[sample(1:length(newx), 10)])
      y_ad = c(y, newy[sample(1:length(newy), 10)])
      z_ad = c(z, newz[sample(1:length(newz), 10)])
      mod_SPLINE_orig = mgcv::gam(z_ad ~ x_ad + y_ad + s(x_ad,y_ad), method="REML")
      mod_TENSOR_orig = mgcv::gam(z_ad ~ x_ad + y_ad + te(x_ad,y_ad), method="REML")
    }, error=function(e){
      x_ad = c(x, newx[sample(1:length(newx), 20)])
      y_ad = c(y, newy[sample(1:length(newy), 20)])
      z_ad = c(z, newz[sample(1:length(newz), 20)])
      mod_SPLINE_orig = mgcv::gam(z_ad ~ x_ad + y_ad + s(x_ad,y_ad), method="REML")
      mod_TENSOR_orig = mgcv::gam(z_ad ~ x_ad + y_ad + te(x_ad,y_ad), method="REML")
    })
  })
  if ((summary(mod_SPLINE)$r.sq - summary(mod_TENSOR)$r.sq) < 10){ ### if the difference in R2 between spline and tensor models is less than 10 then use the spline (because it results in more sensible gradients)
    mod_text_legend = paste0("GAM-spline: R2=", round(summary(mod_SPLINE_orig)$r.sq * 100), "%")
    print(mod_text_legend)
    mod = mod_SPLINE
  } else {
    mod_text_legend = paste0("GAM-tensor: R2=", round(summary(mod_TENSOR_orig)$r.sq * 100), "%")
    print(mod_text_legend)
    mod = mod_TENSOR
  }
  ### interpolate across the landscape using the best gam model
  predx = seq(from=min(newx), to=max(newx), by=0.01) ### for relatively smooth contours
  # predy = seq(from=min(newy), to=max(newy), length=length(predx))
  predy = seq(from=-39.5, to=max(newy), length=length(predx)) ### to encapsulate until the southern tip of Victoria
  predz = predict(mod, newdata=data.frame(newx=rep(predx, each=length(predy)), newy=rep(predy, times=length(predx))))
  Z = matrix(predz, byrow=TRUE, nrow=length(predy))
  # unstandardize Z and adjust minimum to be 0.00
  Z = (Z*sdz)+muz
  Z = Z + abs(min(c(min(Z), 0)))
  ### save gradient data
  saveRDS(list(predx=predx, predy=predy, Z=Z), file=paste0("landscape_surfaces_gradient_data_", herbi, ".rds"))
  ### PLOT
  ncolors = 25 ### 25 is a nice number to see dicrete contour lines across the landscape methinks...
  color_gradient = rev(colorRampPalette(c("#A50026","#D73027","#F46D43","#FDAE61","#FEE08B","#FFFFBF","#D9EF8B","#A6D96A","#66BD63","#1A9850","#006837"))(ncolors))
  png(paste0("Landscape_plot_", herbi, "_resistance.png"), width=1600, height=1100)
    par(orig_par)
    layout(matrix(c(rep(1,5),2),nrow=1))
    par(cex=2)
    ### define x-y limits
    dx=c(min(predx), max(predx))
    dy=c(min(predy), max(predy))
    xaxis = seq(round(min(predx)), round(max(predx)), 2)
    yaxis = seq(round(min(predy))-2, round(max(predy))+2, 2)
    ### prepare the box enclosing the map
    # plot(0, xlim=dx, ylim=dy, asp=1, type="n", xlab="Longitude", ylab="Latitdue", main=paste0(herbi, " Resistance Gradient\n", mod_text_legend), xaxt="n", yaxt="n") #empty
    plot(0, xlim=dx, ylim=dy, asp=1, type="n", xlab="Longitude", ylab="Latitdue", main=paste0(herbi, " Resistance Gradient"), xaxt="n", yaxt="n") #empty
    ### overlay the heatmap
    par(new=TRUE)
    image(x=predx, y=predy, z=Z, col=color_gradient[round(min(Z)*ncolors):round(max(Z)*ncolors)], xlab="", ylab="", main="", asp=1, xaxt="n", yaxt="n")
    # image(x=predx, y=predy, z=Z, col=color_gradient[round(min(Z)*ncolors):round(max(Z)*ncolors)], xlab="", ylab="", main="", asp=1)
    ### extract the bounding box of the map
    outline = maps::map("world", plot=FALSE)
    xrange = range(outline$x, na.rm=TRUE)
    yrange = range(outline$y, na.rm=TRUE)
    xbox = xrange + c(-2, 2)
    ybox = yrange + c(-2, 2)
    ### draw the outline of the map and color the water blue
    polypath(c(outline$x, NA, c(xbox, rev(xbox))),
           c(outline$y, NA, rep(ybox, each=2)),
           col="light blue", rule="evenodd")
    ### name the ticks with degree signs ("\U00B0")
    axis(side=1, at=xaxis, labels=paste0(xaxis, "\U00B0"))
    axis(side=2, at=yaxis, labels=paste0(yaxis, "\U00B0"),las=2)
    ### add a grid 'cause it looks noice
    grid(col="darkgray")
    ### plot points
    par(new=TRUE)
    plot(0, xlim=dx, ylim=dy, asp=1, type="n", xlab="", ylab="", main="", xaxt="n", yaxt="n") #empty
    # plot(0, xlim=dx, ylim=dy, asp=1, type="n", xlab="", ylab="", main="")
    for (i in 1:nrow(filtered)){
      x = filtered$Coordinates_E[i]
      y = filtered$Coordinates_N[i]
      r = (z[i]*sdz)+muz
      if (!is.na(r)){
        points(x, y, col="black", bg=color_gradient[round(r*ncolors)+1], pch=filtered$pch[i])
      }
    }
    ### plot heat map legend
    legend_x=seq(from=0,to=1, length=length(color_gradient))
    legend_y=seq(from=0, to=1, length=length(color_gradient))
    legend_z = matrix(rep(legend_x, times=length(color_gradient)), byrow=TRUE, nrow=length(color_gradient))
    plot(x=c(0,1), y=c(0,1), type="n", xlab="", ylab="", xaxt="n", yaxt="n", main="")
    par(new=TRUE)
    image(x=legend_x, y=legend_y, z=legend_z, col=color_gradient, xlab="", ylab="", main="", xaxt="n", las=2)
    mtext("Completely\nResistant", side=3, line=0.5, at=0.5, cex=2)
    mtext("Completely\nSusceptible", side=1, line=1.5, at=0.5, cex=2)
    ### inset histogram of resistance
    par(fig=c(0,1,0,1))
    par(fig=c(0.01, 0.4, 0.08, 0.5), cex=1, new=TRUE)
    z_orig = (z*sdz)+muz
    nclass=10
    hist(z_orig, ylab= "", xlab="", yaxt="n", main="", nclass=nclass, col=colorRampPalette(color_gradient[round(min(z_orig)*ncolors):round(max(z_orig)*ncolors)])(nclass), bord=FALSE)
    dev.off()
}
