#analysis before we have gathered the final mortality count 20180507

################################################################################################
#load libraries
library(agricolae)
library(ggplot2)
library(stringi)
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

################################################################################################
#load input file
setwd("/home/student.unimelb.edu.au/jparil/Documents/PHENOMICS - herbicide resistance leaf assays/instaGraminoid/MISC/EXP16_OUTPUT/ANALYSIS")
dat = read.csv("./INPUT/EXP16_ANALYSIS_INPUT.csv")
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

################################################################################################
#remove empty pots
d = subset(dat, AREA>0)
d = droplevels(d)
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

################################################################################################
#plots
jpeg("./OUTPUT/BOX AND DESITY PLOTS GREEN METRICS.jpeg", quality=100, width=1500, height=700)
par(mfrow=c(2,3), cex.main=2, cex.axis=1.5)
boxplot(GREEN_INDEX~DAY, d, main="GREEN_INDEX")
boxplot(GREEN_INDEX~HERBI, d, main="GREEN_INDEX")
plot(density(d$GREEN_FRACTION), main="GREEN_INDEX")
boxplot(GREEN_FRACTION~DAY, d, main="GREEN_FRACTION")
boxplot(GREEN_FRACTION~HERBI, d, main="GREEN_FRACTION")
plot(density(d$GREEN_INDEX), main="GREEN_FRACTION")
dev.off()

jpeg("./OUTPUT/BOXPLOTS DAY-HERBI GREEN METRICS.jpeg", quality=100, width=2500, height=800)
par(mfrow=c(2,1), cex.main=2, cex.axis=1.5)
boxplot(GREEN_INDEX~DAY+HERBI, d, main="GREEN_INDEX")
boxplot(GREEN_FRACTION~DAY+HERBI, d, main="GREEN_FRACTION")
dev.off()

# dp <- ggplot(d, aes(x=DAY, y=GREEN_INDEX, fill=DAY)) + 
#   geom_violin(trim=FALSE)+
#   geom_boxplot(width=0.1, fill="white")+
#   labs(x="", y = "GREEN_INDEX")+
#   theme(text=element_text(size=20), axis.text=element_text(size=20), legend.position="none")
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

################################################################################################
#modeling and ranking factors
d.inverleigh = subset(d, POP=="INVERLEIGH")
d.urana = subset(d, POP=="URANA")

DATA = d
METRIC = "GREEN_FRACTION"
FACTOR = c("DAY", "HERBI", "GENO")
NPOOLS=5

mod = eval(parse(text=paste("lm(", METRIC, "~", stri_join_list(list(FACTOR), sep="+"), ", data=DATA)", sep="")))
for(f in FACTOR){
	assign( paste("HSD.", f, sep=""), HSD.test(mod, trt=f) )
}
if(exists("HSD.GENO")){
	GENO=HSD.GENO$groups
	plot(density(GENO[,1]))
	POOLS=rep(1:NPOOLS, each=round(nrow(GENO)/NPOOLS, 1))
	if(length(POOLS)<nrow(GENO)){POOLS=c(POOLS, rep(NPOOLS, times=(nrow(GENO)-length(POOLS))))}
	GENO$POOLS = POOLS
}
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
