#Prepare quantitative resistance estimated per rep
PREPARE_DATA <- function(data_filename, s=FALSE){

	ADDITIONAL_METRICS <- function(DATA, START_METRIC, END_METRIC) {
		#add metric transformations
			#DELTA
			DAYS = levels(dat$DAY)
			nDAYS = length(DAYS)
			METRICS = DATA[, START_METRIC:END_METRIC]
			nMETRICSPerDay = ncol(METRICS) / nDAYS
			for (i in 1:(nDAYS-1)) {
				for (j in (i+1):nDAYS) {
					DELTA = METRICS[, (((j-1)*nMETRICSPerDay)+1):(j*nMETRICSPerDay)] - METRICS[, (((i-1)*nMETRICSPerDay)+1):(i*nMETRICSPerDay)]
					NAMES = paste("DELTA", substr(DAYS[i],4,nchar(DAYS[i])), substr(DAYS[j],4,nchar(DAYS[j])), substr(colnames(DELTA), start=6, stop=nchar(colnames(DELTA))), sep="")
					colnames(DELTA) = NAMES
					DATA = cbind(DATA, DELTA)
				}
			}

			# #PAIRWISE PRODUCTS
			# METRICS = DATA[, START_METRIC:END_METRIC]
			# for (k in 1:(ncol(METRICS)-1)) {
			# 	for (l in (k+1):ncol(METRICS)) {
			# 		PROD = (METRICS[,k])/100 * METRICS[,l]
			# 		NAME = paste("PROD_", colnames(METRICS)[k], "_X_", colnames(METRICS)[l], sep="")
			# 		NAMES = c(colnames(DATA), NAME)
			# 		DATA = cbind(DATA, PROD)
			# 		colnames(DATA) = NAMES
			# 	}
			# }
		return(DATA)
	}

	#import data
	dat = read.csv(data_filename)

	#order
	dat = dat[order(dat$REPLI), ]
	dat = dat[order(dat$PLANT), ]
	dat = dat[order(dat$HERBI), ]
	dat = dat[order(dat$POPUL), ]
	dat = dat[order(dat$DAY), ]

	#isolate factors per measurement time
	ndays = nlevels(dat$DAY)
	POPUL = dat$POPUL[1:(nrow(dat)/ndays)]
	PLANT = dat$PLANT[1:(nrow(dat)/ndays)]
	REPLI = dat$REPLI[1:(nrow(dat)/ndays)]
	HERBI = dat$HERBI[1:(nrow(dat)/ndays)]
	SURVI = dat$SURVI[1:(nrow(dat)/ndays)]

	#merge into a new data set
	DATA = data.frame(POPUL, PLANT, REPLI, HERBI, SURVI)

	#decompose metrics per measurement time and cbind into the new data set
	DAYS = levels(dat$DAY)
	METRICS = colnames(dat)[7:ncol(dat)]
	for (i in DAYS) {
		SUBSET = subset(dat, DAY==i)
		for (j in METRICS) {
			eval(parse(text=paste("DATA$", i, "_", j, " = SUBSET$", j, sep="")))
		}
	}

	#add additional metrics
	DATA = ADDITIONAL_METRICS(DATA=DATA, START_METRIC=6, END_METRIC=ncol(DATA))

	#set infinite values as NA
	METRICS_ALL=colnames(DATA)[6:ncol(DATA)]
	for (m in METRICS_ALL) {
		eval(parse(text=paste("DATA$", m, "[is.infinite(DATA$", m, ")] = NA", sep="")))
	}

	#remove missing pots
	#DATA = DATA[complete.cases(DATA), ]	#remove rows with at least 1 NA #I'm removing too much!! DARN!
	DATA = DATA[DATA$DAY00_AREA>0, ]	#remove empty pots at day 0
	# DATA = DATA[DATA$DAY07_AREA!=0, ]	#remove empty pots at day 7
	# DATA = DATA[DATA$DAY14_AREA!=0, ]	# remove empty pots at dat 14
	DATA = DATA[, colSums(is.na(DATA))==0]

	#scale and center (IS THIS REALLY REQUIRED?!?!?!) 20180227 --> NOPE! SAME OUPUT REGARDLESS OF SCALING/CENTERING 20180302
	if (s==TRUE) {
		START_METRIC = 6
		END_METRIC = ncol(DATA)
		DATA[, START_METRIC:END_METRIC] = scale(DATA[, START_METRIC:END_METRIC], center=T, scale=T)
		#should we also filter-out highly correlated metrics?
	}
	return(DATA)
}

#EXECUTE!
#setup working directory and libraries
work_dir="/home/student.unimelb.edu.au/jparil/Documents/PHENOMICS - herbicide resistance leaf assays/instaGraminoid/misc/EXP16_OUTPUT/ANALYSIS"
setwd(work_dir)
library(glmnet)
library(lme4)
library(popbio) #for logit curve with histograms
#prepare data
DATA = PREPARE_DATA(data_filename="INPUT/EXP16_ANALYSIS_INPUT.csv", s=FALSE)
#extracting the quantitative herbicide resistance estimates from multivariate logistic model predicted values
metrics=colnames(DATA)[6:ncol(DATA)]
X = eval(parse(text=paste("cbind(", paste(paste("DATA$", metrics, sep=""), collapse=","), ")", sep="")))
colnames(X) = metrics
model = glmnet(x=X, y=as.factor(DATA$SURVI), family="binomial", alpha=0.5, nlambda=100) #alpha=0:RR penalty; alpha=1:Lasso penalty
lambdai = model$dev.ratio==max(model$dev.ratio)
QUANT.RES = model$a0[lambdai] + (X%*%model$beta[,lambdai])[,1]
pred = 1/(1+exp(-QUANT.RES))
DATA$QUANT.RES = (QUANT.RES - min(QUANT.RES))/(max(QUANT.RES) - min(QUANT.RES)) #transform so as to range from 0 to 1
	logi.hist.plot(QUANT.RES, DATA$SURVI, boxp=F, type="hist", xlabel="Quantitative Resistance Estimate", ylabel="Survival", mainlabel="Logistic Regression Plot")
	class = ifelse(pred > 0.50, 1, 0); right = sum(class==DATA$SURVI); wrong = sum(class!=DATA$SURVI); perfo = right/sum(right, wrong)
	legend("topleft", legend=paste0(round(perfo*100, 0), "% correctly classified"))
#extract BLUEs per herbicide
for(herbi in levels(DATA$HERBI)){
	blup.mod = lmer(QUANT.RES ~ 0 + (1|REPLI) + PLANT, data=subset(DATA, HERBI==herbi))
	blue = data.frame(fixef(blup.mod))
	rownames(blue) = gsub(pattern="PLANT", replacement="", rownames(blue))
	colnames(blue) = herbi
	eval(parse(text=paste(herbi, "=blue")))
}
#merge
# m1 = merge(SULFO, TERBU, by="row.names"); rownames(m1) = m1$Row.names; m1 = m1[,2:3]
# m2 = merge(GLYPH, TRIFL, by="row.names"); rownames(m2) = m2$Row.names; m2 = m2[,2:3]
# MERGED = merge(m1, m2,  by="row.names")
# colnames(MERGED) = c("PLANT", "SULFO", "TERBU", "GLYPH", "TRIFL")
#write-out
for (i in c("SULFO", "TERBU", "GLYPH", "TRIFL")){
	write.csv(eval(parse(text=i)), file=paste0("OUTPUT/QUANTITATIVE RESISTANCE FOR POOLING-", i,".csv"))
}
