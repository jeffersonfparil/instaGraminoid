### Read a directory and extract shoot and root areas

### import libraries
import os, sys, time

### extract directory containing the scripts
dir_src = os.path.dirname(sys.argv[0])
### extract the input directory from user, if empty then we assume it is the current directory
try:
    dir_input = sys.argv[1]
except:
    dir_input = "."
### extract image extension name or filename suffix, if empty we defult to "jpg"
try:
    extension_name = sys.argv[2]
except:
    extension_name = "jpg"
### extract output directory from user, if empty then create one in the input directory
try:
    dir_output = sys.argv[3]
except:
    dir_output = dir_input + "/OUTPUT-" + time.strftime("%Y-%m-%d-%H-%M")
    os.mkdir(dir_output)
### load functions
exec(open(os.path.join(dir_src, "functions.py")).read())
### list filenames of the input images
filename_list = [os.path.join(dir_input, f) for f in os.listdir(dir_input) if (f.endswith('.' + extension_name)) & (f.find("-PHOTO_1") >- 1)]
### extract shoot and root areas iteratively (or in parallel)
time_start = time.time()
for fname in filename_list:
    print(fname)
    fun_extract_metrics(fname=fname,
                        dir_output=dir_output,
                        write_out=True,
                        plot_out=True)
time_end = time.time()
print("Time elapsed: ", str(round(time_end - time_start)) + " seconds")

# # ### test
# DIR=/home/jeff/Downloads/weedomics_large_files/1.a-photos-day_14/TAPO
# SRC_DIR=/home/jeff/Documents/instaGraminoid/misc/hyperspectral_2D_imaging
# time python ${SRC_DIR}/main.py ${DIR} jpg
# ### merge output
# echo "ID,GREEN,RED,NDVI_ambient,NDVI_illuminated" > ${DIR}/merged.csv
# cat ${DIR}/OUTPUT-*/*.csv >> ${DIR}/merged.csv

# ### In R
# cd $DIR
# R

# dat = read.csv("merged.csv")
# mat_ID = matrix(unlist(strsplit(dat$ID, "-")), ncol=3, byrow=TRUE)
# dat$TRAY = mat_ID[,1]
# dat$DAY = mat_ID[,3]

# dat = dat[order(dat$TRAY, decreasing=FALSE), ]
# dat = dat[order(dat$DAY, decreasing=FALSE), ]
# dat$POP = rep(c("ACC59", "ACC62"), each=14)
# dat$TRT = rep(head(rep(c("Clethodim", "Atrazine", "Paraquat", "Glyphosate", "Control"), each=3), -1), times=2)

# library(violinplotter)

# svg("NDVI_ambient-D14.svg", width=12, height=10)
# violinplotter(NDVI_ambient ~ POP * TRT, data=dat)
# dev.off()

# svg("NDVI_illuminated-D14.svg", width=12, height=10)
# violinplotter(NDVI_illuminated ~ POP * TRT, data=dat)
# dev.off()

# svg("Green_channel-D14.svg", width=12, height=10)
# violinplotter(GREEN ~ POP * TRT, data=dat)
# dev.off()
