### Test hyperpectral analysis and also plan on restructuring this whole repo for the better :-P

### Testing tapo-generated low-res RGB + night vision (~845nm NIR) photos 2021-09-01

import sys, os
import numpy as np
import scipy
from scipy import ndimage
import skimage
from skimage import io
from skimage import segmentation
import cv2
import matplotlib; matplotlib.use('TkAgg')
# import matplotlib; matplotlib.use('Agg') #to prevent errors in MacOS compilation
from matplotlib import pyplot as plt



### plot image
def fun_image_show(image, cmap=['viridis','gray','Pastel1', 'hsv'][0], title="", width=5, height=5, save=False):
    ######################################
    ### TEST:
    # image = io.imread('res/Lolium/1-d7.jpg')
    # fun_image_show(image)
    # fun_image_show(image[:,:,0])
    # fun_image_show(image[:,:,0], cmap="Pastel1")
    # fun_image_show([io.imread('res/Lolium/1-d7.jpg'), io.imread('res/Lolium/10-d7.jpg')])
    # fun_image_show([io.imread('res/Lolium/1-d7.jpg')[:,:,0], io.imread('res/Lolium/10-d7.jpg')[:,:,1]])
    # fun_image_show([io.imread('res/Lolium/1-d7.jpg')[:,:,0], io.imread('res/Lolium/10-d7.jpg')[:,:,1]], cmap=["gray", "Pastel2"])
    ######################################
    if isinstance(image, np.ndarray):
        image = [image]
    ncol = np.int(np.ceil(np.sqrt(len(image))))
    nrow = np.int(np.ceil(len(image) / ncol))
    fig = plt.figure(figsize=(width*ncol, height*nrow))
    for i in range(len(image)):
        fig.add_subplot(nrow, ncol, i+1)
        if isinstance(title, list):
            plt.title(title[i])
        else:
            plt.title(title)
        nDims = len(image[i].shape)
        if nDims==3:
            plt.imshow(image[i])
        elif nDims==2:
            try:
                plt.imshow(image[i], cmap=cmap[i])
            except:
                plt.imshow(image[i], cmap=cmap)
        else:
            return(1)
    if save == False:
        try:
            plt.show(block=False)
        except:
            plt.show()

### crop to include only the plate
def fun_image_crop(image, x0, x1, y0, y1, plot=False):
    ############################
    ### TEST:
    # image = io.imread('res/Lolium/1-d7.jpg')
    # x0 = 1200
    # x1 = 3100
    # y0 = 1900
    # y1 = 3800
    # plot=True
    # fun_image_crop(image, x0, x1, y0, y1, plot)
    ############################
    X = np.copy(image[y0:y1, x0:x1]) ### where ys are the rows and xs are the columns
    if plot: fun_image_show(X)
    if X.size == 0:
        print("Incorrect cropping coordinates!")
        return(None)
    return(X)

### add colour correction or white-balancing function
def fun_white_balance(image, method=["white_patch", "gray_world"][0], perc=99):
    ##############################
    ### TEST:
    # image = io.imread('res/Lolium/1-d7.jpg')
    # method = "white_patch"
    # perc = 99
    # image_WP = fun_white_balance(image, "white_patch", perc=99)
    # image_GW = fun_white_balance(image, "gray_world")
    # fun_image_show([image, image_WP, image_GW])
    ##############################
    if method=="white_patch":
        ### white-patch colour-correction
        ### pixel values at the perc-th percentile
        vec_denom_pixel = np.percentile(image, perc, axis=(0, 1))
    elif method=="gray_world":
        ### grayworld colour-correction
        ### pixel values at the mean channel values
        vec_denom_pixel = image.mean(axis=(0,1))
    else:
        print("Please select a method from:")
        print("   - 'white_patch'")
        print("   - 'gray_world'")
        return(0)
    mat_01 = image/255
    vec_correction_factor = [255,255,255]/vec_denom_pixel
    mat_corrected = (mat_01 * vec_correction_factor).clip(0, 1)
    image_corrected = skimage.img_as_ubyte(mat_corrected)
    return(image_corrected)

### flatten RGB images, i.e. transform the 3D array into a 2D array by grayscale transformation of extracting one of the 3 colour channels
def fun_image_flatten(image, vec_coef=[0.2125, 0.7154, 0.0721], plot=False):
    ########################################################################
    ### TEST:
    # image = io.imread('res/Lolium/1-d7.jpg')
    # # vec_coef = [0.2125, 0.7154, 0.0721] ### grayscale transformation
    # # vec_coef = [1.0, 0.0, 0.0] ### extract red channel
    # vec_coef = [0.0, 1.0, 0.0] ### extract green channel
    # plot = True
    # fun_image_flatten(image, vec_coef, plot)
    ########################################################################
    ### for each pixel: multiply each colour channel value with the coefficients,
    ### and take the sum
    X = np.sum(image *  vec_coef, 2)
    if plot: fun_image_show(X)
    return(X)

### compute the segmentation threshold using one of 7 algorithms
def fun_threshold_compute(flatImage, algo=["isodata", "li", "mean", "minimum", "otsu", "triangle", "yen"][0]):
    ##########################################################################################################
    ### TEST:
    # image = io.imread('res/Lolium/1-d7.jpg')
    # flatImage = fun_image_flatten(image, [0.0, 1.0, 0.0], False)
    # fun_threshold_compute(flatImage)
    # fun_threshold_compute(flatImage, algo="triangle")
    ##########################################################################################################
    if (algo=="isodata"):
        out = skimage.filters.threshold_isodata(flatImage)
    elif (algo=="li"):
        out = skimage.filters.threshold_li(flatImage)
    elif (algo=="mean"):
        out = skimage.filters.threshold_mean(flatImage)
    elif (algo=="minimum"):
        out = skimage.filters.threshold_minimum(flatImage)
    elif (algo=="otsu"):
        out = skimage.filters.threshold_otsu(flatImage)
    elif (algo=="triangle"):
        out = skimage.filters.threshold_triangle(flatImage)
    elif (algo=="yen"):
        out = skimage.filters.threshold_yen(flatImage)
    else:
        return(1)
    return(out)

### select the best threshold among the thresholding algorithms
### given a maximum detection proportion threshold: maxFrac
def fun_threshold_optim(flatImage, maxFrac=0.10, plot=False):
    #############################################
    ### TEST:
    # image = io.imread('res/Lolium/1-d7.jpg')
    # flatImage = fun_image_flatten(image, [0.0, 1.0, 0.0], False)
    # maxFrac = 0.10
    # plot = True
    # fun_threshold_optim(flatImage, maxFrac, plot)
    #############################################
    nElements = flatImage.shape[0] * flatImage.shape[1]
    vec_algos = np.array(["isodata", "li", "mean", "minimum", "otsu", "triangle", "yen"])
    vec_fracRetained = np.array([])
    for algo in vec_algos:
        x = fun_threshold_compute(flatImage, algo)
        fracRetained = np.sum(flatImage >= x) / nElements
        vec_fracRetained = np.append(vec_fracRetained, fracRetained)
    idx = vec_fracRetained < maxFrac
    ### if none of the algorithms pass the maxFrac threshold then use the algorithm which minimised the fraction of pixels retained
    if np.sum(idx) == 0: idx = vec_fracRetained == np.min(vec_fracRetained)
    ### choose the least conservative algorithm
    algo = vec_algos[idx][vec_fracRetained[idx]==np.max(vec_fracRetained[idx])][0]
    threshold = fun_threshold_compute(flatImage, algo)
    if plot:
        skimage.filters.thresholding.try_all_threshold(flatImage)
        try:
            plt.show(block=False)
        except:
            plt.show()
    return(threshold, algo)

### background suubtraction
def fun_background_subtraction(RGBImage, maxFrac=0.10, plot=False):
    #############################
    ## TEST:
    # fname = 'TRAY_01-PHOTO_1-DAY_14.jpg'
    # rawImage  = io.imread(fname)
    # whiteBalanced = fun_white_balance(rawImage)
    # cropped = fun_image_crop(whiteBalanced, x0=800, x1=1500, y0=250, y1=800)
    # fun_background_subtraction(cropped, plot=True)
    #############################
    flattened = fun_image_flatten(RGBImage)
    thresh, _ = fun_threshold_optim(flattened, maxFrac=maxFrac)
    mask = flattened>=thresh
    X = np.copy(RGBImage)
    X[np.invert(mask),:] = 0
    if plot:
        fun_image_show([RGBImage, flattened, mask, X])
    return(X, mask, thresh)

### extract and save metrics
def fun_extract_metrics(fname, dir_output=".", plot_out=False, write_out=True):
    ####################################
    ### TEST:
    # fname = "TRAY_01-PHOTO_1-DAY_14.jpg"
    # fname = "TRAY_07-PHOTO_1-DAY_14.jpg"
    # fun_extract_metrics(fname, plot=False)
    ####################################
    ### image extension name or filename suffix
    extension_name = fname.split(".")[-1]
    ### output filenames
    fname_base = os.path.basename(fname)
    if dir_output==".":
        dir_output = os.path.dirname(fname)
    fname_out_csv = os.path.join(dir_output, fname_base.replace("." + extension_name, "-metrics.csv"))
    fname_out_flat = os.path.join(dir_output, fname_base.replace("." + extension_name, "-processing.jpg"))
    fname_out_hist = os.path.join(dir_output, fname_base.replace("." + extension_name, "-histograms.jpg"))
    ### load
    raw_RGB  = io.imread(fname)
    raw_NIR0 = io.imread(fname.replace("-PHOTO_1-", "-PHOTO_2-"))
    raw_NIR1 = io.imread(fname.replace("-PHOTO_1-", "-PHOTO_3-"))
    ### crop
    crp_RGB = fun_image_crop(raw_RGB, x0=800, x1=1500, y0=250, y1=800)
    crp_NIR0 = fun_image_crop(raw_NIR0, x0=800, x1=1500, y0=250, y1=800)
    crp_NIR1 = fun_image_crop(raw_NIR1, x0=800, x1=1500, y0=250, y1=800)
    ### white-balance
    wb_RGB = fun_white_balance(crp_RGB)
    wb_NIR0 = fun_white_balance(crp_NIR0)
    wb_NIR1 = fun_white_balance(crp_NIR1)
    ### background subtraction
    _, mask, _ = fun_background_subtraction(wb_RGB, maxFrac=0.50)
    wb_RGB[np.invert(mask),:] = 0
    wb_NIR0[np.invert(mask),:] = 0
    wb_NIR1[np.invert(mask),:] = 0
    ### flatten
    GREEN = fun_image_flatten(wb_RGB, vec_coef=[0.0, 1.0, 0.0])
    RED = fun_image_flatten(wb_RGB, vec_coef=[1.0, 0.0, 0.0])
    NIR_0 = fun_image_flatten(wb_NIR0)
    NIR_1 = fun_image_flatten(wb_NIR1)
    NDVI_0 = (NIR_0-RED)/(NIR_0+RED+1e-10)
    NDVI_1 = (NIR_1-RED)/(NIR_1+RED+1e-10)
    ### plot
    if plot_out:
        fun_image_show([raw_RGB, crp_RGB, wb_RGB, RED, GREEN, NIR_0, NIR_1, NDVI_0, NDVI_1], title=["Raw", "Cropped", "White balanced", "Red Channel", "Green Channel", "NIR ambient light", "NIR infrared illumination", "NDVI using NIR ambient", "NDVI using infrared LED"], save=plot_out)
        plt.savefig(fname_out_flat)
        plt.close()
        width = 8
        height = 5
        ncol = 2
        nrow = 3
        fig = plt.figure(figsize=(width*ncol, height*nrow))
        fig.add_subplot(nrow, ncol, 1); plt.title('Green channel')
        plt.hist(GREEN.ravel()); plt.grid()
        fig.add_subplot(nrow, ncol, 2); plt.title('Red channel')
        plt.hist(RED.ravel()); plt.grid()
        fig.add_subplot(nrow, ncol, 3); plt.title('NIR ambient light')
        plt.hist(NIR_0.ravel()); plt.grid()
        fig.add_subplot(nrow, ncol, 4); plt.title('NIR infrared LED illuminated')
        plt.hist(NIR_1.ravel()); plt.grid()
        fig.add_subplot(nrow, ncol, 5); plt.title('NDVI ambient NIR')
        plt.hist(NDVI_0.ravel()); plt.grid()
        fig.add_subplot(nrow, ncol, 6); plt.title('NDVI illuminated NIR')
        plt.hist(NDVI_1.ravel()); plt.grid()
        plt.savefig(fname_out_hist)
        plt.close()
    ### output metrics
    mean_green = np.mean(GREEN.ravel())
    mean_red = np.mean(RED.ravel())
    mean_ndvi0 = np.mean(NDVI_0.ravel())
    mean_ndvi1 = np.mean(NDVI_1.ravel())
    dic_out = {'GREEN_mu':mean_green,
               'RED_mu':mean_red,
               'NDVI0_mu':mean_ndvi0,
               'NDVI1_mu':mean_ndvi1}
    ### write output
    col_ID = fname_base.replace("."+extension_name, "")
    vec_keys = list(dic_out.keys())
    vec_vals = list(dic_out.values())
    dtype=[('ID', 'U24'), ('GREEN', 'f4'), ('RED', 'f4'), ('NDVI_ambient', 'f4'), ('NDVI_illuminated', 'f4')]
    arr_out = np.array([(col_ID, mean_green, mean_red, mean_ndvi0, mean_ndvi1)], dtype=dtype)
    if write_out:
        np.savetxt(fname=fname_out_csv, X=arr_out, delimiter=',', fmt=['%s', '%f', '%f', '%f', '%f'], comments='')
    ### return
    return(arr_out)

