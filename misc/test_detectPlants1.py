#!/usr/bin/env python
import os, sys
import cv2
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.integrate import simps
import scipy.stats as stats
from scipy.stats import norm
from scipy.optimize import minimize
import sys

#import the image
filename = sys.argv[1]
img = cv2.imread(filename)
nrows, ncols, nchannels = img.shape

#crop
img = img[600:2400,:]

#white balancing
SET_WHITE = 140
mean_column = np.mean(img[0:200,0:200], axis=0) #140:300, 2:220
mean_row = np.mean(mean_column, axis=0)
CORRECTION_FACTOR = SET_WHITE/mean_row #setting the white styrofoam box color to 140-140-140
img = np.uint8(CORRECTION_FACTOR*img)

#compute mean pixel values
blue = np.mean(img[:,:,0])
green = np.mean(img[:,:,1])
red = np.mean(img[:,:,2])
OUT = np.array([blue, green, red])
OUT = OUT.reshape((1,3))

#write
np.savetxt(fname="OUT-" + filename.split(".",1)[0] + ".csv", X=OUT, delimiter=',')
cv2.imwrite("OUTPUT/" + filename.split(".",1)[0] + '-out.jpg', img)

#execute in bash
# mkdir /path/to/images/OUTPUT/
# chmod +x test_detectPlants1.py
# nohup parallel ./test_detectPlant.py {} ::: $(ls /path/to/images/ | grep .JPG) &
# cat OUT-*.csv > OUT.temp
# rm OUT-*.csv
# ls | grep .JPG > column1.temp
# paste -d, column1.temp OUT.temp > OUT.csv
# rm *.temp

# R
# dat = read.csv("OUT.csv", header=F)
# plot(dat$V3, dat$V4)
