# ggplot2 drawing plots and custom legend with rectangles and text manually 20180303

library(ggplot2)
len = 1000
from=-3
to=3
npools = 5

x = seq(from=from, to=to, length.out=len)
y = dnorm(x)
df = data.frame(x,y)

q = quantile(x, probs=seq(0,1,length.out=npools+1)) #divide x equally which is not what we want. we want equal number of individuals per pool
f = seq(from=0, to=1, length.out=npools+1)
q = qnorm(f); q[1] = min(x); q[length(q)] = max(x)

col = c("#D55E00", "#E69F00", "#009E73", "#56B4E9", "#CC79A7")

jpeg("Density Plot 5 Pools.jpeg", quality=100, width=700, height=500)
qplot(x,y,data=df,geom="line")+
	geom_ribbon(data=subset(df ,x>q[1] & x<q[2]),aes(ymax=y),ymin=0,fill=col[1],colour=NA,alpha=0.5) +
	geom_ribbon(data=subset(df ,x>q[2] & x<q[3]),aes(ymax=y),ymin=0,fill=col[2],colour=NA,alpha=0.5) +
	geom_ribbon(data=subset(df ,x>q[3] & x<q[4]),aes(ymax=y),ymin=0,fill=col[3],colour=NA,alpha=0.5) +
	geom_ribbon(data=subset(df ,x>q[4] & x<q[5]),aes(ymax=y),ymin=0,fill=col[4],colour=NA,alpha=0.5) +
	geom_ribbon(data=subset(df ,x>q[5] & x<q[6]),aes(ymax=y),ymin=0,fill=col[5],colour=NA,alpha=0.5) +
	geom_rect(aes(xmin=2.0, xmax=2.2, ymin=0.38, ymax=0.40), fill=col[1]) +
	geom_rect(aes(xmin=2.0, xmax=2.2, ymin=0.36, ymax=0.38), fill=col[2]) +
	geom_rect(aes(xmin=2.0, xmax=2.2, ymin=0.34, ymax=0.36), fill=col[3]) +
	geom_rect(aes(xmin=2.0, xmax=2.2, ymin=0.32, ymax=0.34), fill=col[4]) +
	geom_rect(aes(xmin=2.0, xmax=2.2, ymin=0.30, ymax=0.32), fill=col[5]) +
	annotate("text", x=2.5, y=c(0.39, 0.37, 0.35, 0.33, 0.31), label=paste("Pool", 1:npools, sep="")) +
	labs(x="Herbicide Resistance", y="Density")
dev.off()
