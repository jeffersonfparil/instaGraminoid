#check for missed photographs after openCV_plant_colorimetric.py METRICS EXTRACTION ---> I don't know why it spits out errors maybe due to gnu-parallel???!!!

DIR = "/mnt/EXP16/SULFO/INPUT/OUTPUT"

setwd(DIR)
dat = read.csv("OUTPUT.csv")
x = paste("./", dat$DAY,"-", dat$TRAY, "-", dat$CELL, "-", dat$REP, ".jpg", sep="")
y = y = read.delim("../images.temp")

OUT = y[!(y[,1]%in%x), 1]

OUT = gsub("./DAY", "IMAGES/DAY", OUT)

write.table(OUT, file="missedImages.temp", sep="\t", row.names=F, col.names=F, quote=F)

# #THEN IN BASH
# cd $DIR
# nohup parallel ~/instaGraminoid/openCV_plant_colorimetric.py {} 150 112 ::: $(cat missedImages.temp) &